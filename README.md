# KQuickComponents
A series of QML import plugins that leverages KDE Frameworks 5

## Introduction
Components offered:
* dirmodel : use KIO to browse directories
* draganddrop : QML access to the Qt drag and drop system
* kquickcontrols : 
* qtextracomponents : QML bindings to other Qt functionality, such as QPixmap, QImage, QIcons


## Links

- Wiki: <http://community.kde.org/Plasma#Plasma_Workspaces_2>
- Mailing list: <https://mail.kde.org/mailman/listinfo/kde-core-devel>
- IRC channel: #kde-devel on Freenode
- Git repository: <>
